VimHUD is used an addon for Vim built in python and VimScript, that recommends commands for you to learn. VimHUD remembers which commands you know and can and recommends new commands on the bottom of your screen.

Installation: The easiest way to install is to use pathogen. After installing pathogen clone this repo into your .vim/bundle folder. After this, run the following shell script:

source ~/.vim/bundle/VimParser/install.sh

or just run the commands seperately. This is needed for VimHUD to record you keystrokes while you are in vim. This may change in the future if we find a better way of doing this.

Note: you may need to update config.py if you are not using pathogen.

After that, just launch vim and you'll be getting suggestions in no time! To increase or decrease the rate of suggestion, you can do

:set updatetime=2000

Lower means faster updates, and higher means slower updates. We are still experimenting with the default value.

Please understand that we are still in a development state, so please bare with us and report any bugs :)


Dependencies:
vim-stream: https://www.npmjs.com/package/vim-stream
Python 2.7


Details:

Commands are tracked and stored in a yaml file for example:  
- datetime: 2018-07-01 20:01:16.820132  
   keys: {',': 11, /: 1, ':': 13, C: 21, H: 7, I: 13, M: 14, Z: 1, ^: 52, a: 13, b: 3,  
   c: 7, e: 4, j: 132, k: 151, l: 7, n: 4, o: 1, q: 10, r: 4, u: 2, w: 5}  
- datetime: 2018-07-01 20:02:48.848698  
    keys: {$: 3, (: 1, ',': 14, ':': 6, C: 10, F: 2, I: 3, M: 7, Z: 1, ^: 21, a: 6,  
    b: 1, c: 4, d: 4, e: 5, f: 1, h: 1, j: 51, k: 40, l: 57, n: 3, o: 2, p: 4, q: 4,  
    r: 1, s: 1, t: 1, u: 2, v: 3, w: 6, x: 1, y: 2}  

Commands are recommended based off of frequency of use and its usefulness.
