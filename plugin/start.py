import vim


bl = vim.buffers
for buff in bl:
    if str(buff) == '<buffer HUD>':
        HUD = buff
        HUD.append("j=down | k=up | l=right | h=left | Esc=normal mode | i=insert mode")
        break
