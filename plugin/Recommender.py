import config
import random


def create_pdf(freqs, useful):
    total = 0
    for key, freq in freqs.items():
        total += freq
    probabilities = list()
    for key in freqs.keys():
        freqFactor = 1-float(freqs[key])/total
        usefulFactor = useful.get(key, 0)
        probabilities.append(freqFactor+usefulFactor)
    return probabilities


def sample_commands(probabilities, keyList):
    for i in range(1, len(probabilities)):
        probabilities[i] = probabilities[i-1] + probabilities[i]
    rv = random.uniform(0, probabilities[len(probabilities)-1])
    if rv <= probabilities[0]:
        return keyList[0]
    for i in range(1, len(probabilities)-1):
        if probabilities[i-1] < rv and probabilities[i] >= rv:
            return keyList[i]
    return keyList[len(probabilities)-1]


def recommend(freqs, useful, desc):
    keyList = list(freqs.keys())
    pdf = create_pdf(freqs, useful)
    if len(freqs) == 0:
        return "ERROR: no frequencies"
    random = None
    while random not in desc:
        random =  sample_commands(pdf, keyList)
    return str(random) + ": " + str(desc[random])



if __name__=="__main__":
    load_usefull()
