import config
import Session
import Recommender
import os
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt


key = "I"

session = Session.Session()
if os.path.isfile(config.SESSIONS):
    session.loadYaml(config.SESSIONS)
else:
    exit()
session = session.getDict()
x = []
y = []
for s in session:
    if len(s['keys']) == 0:
        continue
    x.append(str(s['datetime'])[:10])
    y.append(s['keys'].get(key, 0))
if len(x) == 0:
    exit()
plt.plot(range(len(y)), y)
plt.xticks(range(len(x)), x)
plt.ylabel('Used')
plt.title('Usage')
plt.show()
