import vim
import os


try:
    import Recommender
    import Session
    import config
    bl = vim.buffers
    for buff in bl:
        if str(buff) == '<buffer HUD>':
            HUD = buff
    HUD.append(config.BASIC)
    del HUD[0]
    del HUD[0]
    session = Session.Session()
    if os.path.isfile(config.SESSIONS):
        session.loadYaml(config.SESSIONS)
    if os.path.isfile(config.LOG):
       session.processVimData(config.LOG)
       session.writeYaml(config.SESSIONS)
       os.remove(config.LOG)
    desc = config.read_yaml(config.COMMANDS)
    useful = config.read_yaml(config.USEFULNESS)
    x = Recommender.recommend(session.getTotalDict(),
                              useful,
                              desc)
    HUD.append("%s"%x)
except ImportError:
    print("need recommender in pythonpath")
