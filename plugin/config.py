import os
import yaml


VIM_STREAM = os.path.expanduser("~/vim-stream/bin/vim-stream")
LOG = os.path.expanduser("~/.vimlog")
SESSIONS = os.path.expanduser("~/.vim/bundle/VimHUD/plugin/config/sessions.yaml")
USEFULNESS = os.path.expanduser("~/.vim/bundle/VimHUD/plugin/config/usefulness.yaml")
COMMANDS = os.path.expanduser("~/.vim/bundle/VimHUD/plugin/config/command.yaml")
PROCESSOR = os.path.expanduser("~/.vim/bundle/VimHUD/plugin/processVimDump.sh")
BASIC = "j=down | k=up | l=right | h=left | Esc=normal mode | i=insert mode"


def read_yaml(fn):
    data = open(fn, 'r').read()
    description = yaml.load(data)
    return description

