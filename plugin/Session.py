import config
import subprocess
import datetime
import yaml


class Session(object):
    def __init__(self):
        self.sessions = list()

    def processVimData(self, fn):
        newSession = dict()
        currentTime = datetime.datetime.now()
        newSession["datetime"] = currentTime
        command = 'sh ' + config.PROCESSOR + ' ' + fn + ' ' + config.VIM_STREAM
        output = subprocess.Popen(command,
                                  shell=True,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE,
                                  stdin=subprocess.PIPE).stdout.read()
        output = output.decode('utf-8').lstrip().rstrip()
        keys = dict()
        for line in output.split("\n"):
            line = line.rstrip().lstrip()
            token = line.split(" ")
            if len(token) == 2:
                freq = token[0]
                command = token[1]
                keys[command] = int(freq)
        newSession["keys"] = keys
        self.sessions.append(newSession)

    def loadYaml(self, fn):
        with open(fn, 'r') as f:
            data = f.read()
        newSessions = yaml.load(data)
        if newSessions is not None:
            self.sessions += newSessions

    def writeYaml(self, fn):
        with open(fn, 'w') as f:
            yaml.dump(self.sessions, f)

    def getDict(self):
        return self.sessions

    def getTotalDict(self):
        total = dict()
        for session in self.sessions:
            for key, freq in session["keys"].items():
                if key in total:
                    total[key] += freq
                else:
                    total[key] = freq
        return total

if __name__=='__main__':
    s = Session()
    s.loadYaml(config.SESSIONS)
    s.processVimData(config.LOG)
    s.writeYaml(config.SESSIONS)
