LOG=$1
VIMSTREAM=$2
cat $LOG | $VIMSTREAM |  sed -e 's/\(.\)/\1\n/g' | sort | uniq -c | sort -nr
