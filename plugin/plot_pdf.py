import config
import Session
import Recommender
import os
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt


session = Session.Session()
if os.path.isfile(config.SESSIONS):
    session.loadYaml(config.SESSIONS)
else:
    exit()
useful = config.read_yaml(config.USEFULNESS)
totals = session.getTotalDict()
pdf = Recommender.create_pdf(totals,
                             useful)
plt.bar(range(len(pdf)), pdf, align="center")
plt.xticks(range(len(pdf)), list(totals.keys()))
plt.ylabel('weight')
plt.title('Recommendation Rank')

plt.show()

