function! Create()
:belowright split HUD
:set nonu
:31winc-
:set laststatus=0
:setlocal buftype=nofile
:call Start()
endfunction

function! Start()
let start = join(readfile(expand('~/.vim/bundle/VimHUD/plugin/start.py')), "\n")
execute 'py3 ' . start
endfunction 

function! Update()
let update = join(readfile(expand('~/.vim/bundle/VimHUD/plugin/update.py')), "\n")
execute 'py3 ' . update
endfunction 

autocmd CursorHold * call Timer()
function! Timer()
call feedkeys("f\e")
call Update()
endfunction

:call Create()
